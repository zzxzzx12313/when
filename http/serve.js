// disk cache  硬盘缓存 第一次请求走disk
// memory cache 内存缓存 会话进程结束内存释放

// http 缓存不会缓存接口 一般会缓存静态资源文件（某一段时间不会变化的资源）

// 协商缓存

// 1. etag 设置版本号
// 2. last-modifed  比较文件最后一次修改时间

const http = require('http');
const fs = require('fs');
const path = require('path');
const plulicPaht = path.resolve('./public');
const url = require('url');
const filePath = (Filename) => path.join(plulicPaht,Filename);
http.createServer((req,res) => {
  const {pathname,query} = url.parse(req.url,true);
  console.log(`req url: ${pathname}`)
  if(pathname === '/favicon.ico'){
    res.end('');
    return;
  }
  if(pathname === '/'){
    res.end(fs.readFileSync(filePath('index.html')));
    return;
  }

  if(/\.js$/.test(pathname)){
    const _etage = query['_'];
    // console.log(_etage,query);
    const jsFilePath = filePath('./js/'+pathname);
    // console.log(jsFilePath);
    if(fs.existsSync(jsFilePath)){
      const state = fs.statSync(jsFilePath);
      if(req.headers['if-none-match'] === _etage){
        res.statusCode = 304;
        res.end();
        return;
      }
      // console.log(req.headers);
      if(req.headers['if-modified-since'] === state.mtime.toGMTString()){ // 文件没有变化
        res.statusCode = 304;
        res.end();
      } else {
        // 第二次发请求 前端携带If-Modified-Since
        res.setHeader('expires',new Date(Date.now() + 10000).toGMTString());
        res.setHeader('cache-control','max-age=0'); // 服务在响应时设置http头
        res.setHeader('last-modified',state.mtime.toGMTString());  // 第一次服务端设置last-modified
        res.setHeader('etag',_etage);
        // http2.0 cache-control 强缓存 max-age 最大缓存时间 相对第一次请求时间
        // http1.0 expires GMT 时间字符串 绝对时间  9.8  容易出现时间误差
        res.end(fs.readFileSync(jsFilePath));
      }
      return;
    }
    res.statusCode = 404;
    res.end();
    return;
  }
}).listen(3000,() => {
  console.log('prot is 3000');
})