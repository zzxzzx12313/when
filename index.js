//数组去重
// let nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]

// function arr(nums) {
//     let numss=[];
//     for (let i = 0; i < nums.length; i++) {
//         if (numss.indexOf(nums[i])==-1) {
//             numss.push(nums[i])
//         }
//     }
//     console.log(numss.length);
//     return numss
// }

// console.log(arr(nums));

//数组里两个数之和

// let num = [7, 11, 15, 2];
// let target = 9;
// // let num = [3,2,4], target = 6

// function arr(num, target) {
//     // console.log(num, target);
//     for (let i = 0; i < num.length; i++) {
//         // console.log(num[i]);
//         for (j = i + 1; j < num.length; j++) {
//             if (num[i] + num[j] === target) {
//                 console.log(i, j);
//             }
//         }
//     }
// }
// arr(num, target)


//数组去重最多两个

// let nums = [1, 1, 1, 2, 2, 3, 3, 4, 5, 6, 6, 7, 7, 8, 8]

// function arr(nums) {
//     for (var i = 0; i < nums.length; i++) {
//         for (var j = i + 2; j < nums.length; j++) {
//             if (nums[i] == nums[j]) {         //第一个等同于第二个，splice方法删除第二个
//                 nums.splice(j, 1);
//                 j--;
//             }
//         }
//     }
//     console.log(nums);
//     return nums;
// }
// arr(nums)


// //整数反转

// let x = -12;
// function reverse(x) {
//     // console.log(x);
//     //数字转成字符串
//     var x = x + "";//"123"
//     //字符串转数组
//     x = x.split('');//[ '1', '2', '3' ]
//     var a = [];
//     var b = 0;
//     for (var i = x.length - 1; i >= 0; i--) {
//         a[b] = x[i];//3  2  1
//         b++;
//     }
//     for (var j = 0; j < a.length; j++) {
//         //判断是负数的时候
//         if (a[j] == "-") {
//             a.splice(j, 1);
//             a.unshift("-");
//         }
//     }
//     if (a[0] == "0" && a[i] <= "9") {
//         a.splice(0, 1);
//         // console.log(a);
//     }
//     //数组转字符串
//     a = a.join("");
//     //字符串转数字
//     a = parseFloat(a);
//     // console.log(a);
//     if (a >= Math.pow(2, 31) - 1 || a <= Math.pow(-2, 31)) {
//         return 0;
//     } else {
//         return a
//     }
// }

// console.log(reverse(x));




// //回文数
// let x = 12121111
// function palindromic(x) {
//     // console.log(x);
//     if (x === 0) {
//         return true
//     }
//     if (x < 0) {
//         return false
//     }
//     let leftArr = [];
//     let rightArr = [];
//     //数字转字符串再转数组
//     const tolArr = x.toString().split("")
//     //获取数组的长度
//     const len = tolArr.length
//     const tolNum = Math.floor(len / 2);
//     // console.log(tolNum);
//     console.log(len % 2);
//     if (len % 2 === 0) {
//         leftArr = tolArr.splice(0, tolNum);
//         // console.log(leftArr);
//         rightArr = tolArr;
//         // console.log(rightArr);
//         leftStr = leftArr.toString();
//         rightStr = rightArr.reverse().toString();
//     }
//     if (len % 2 !== 2) {
//         const tolNum = Math.floor(len / 2);
//         leftArr = tolArr.splice(0, tolNum);
//         rightArr = tolArr.slice(1, tolArr.length)
//         leftStr = leftArr.toString();
//         rightStr = rightArr.reverse().toString();
//     }
//     if (leftStr === rightStr) {
//         return true
//     } else {
//         return false
//     }
// }
// palindromic(x);




// 罗马数字转整数

// let s = "MCMXCIV"
// function romanToInt(s) {
//     // console.log(s);
//     let obj = {
//         I: 1,
//         V: 5,
//         X: 10,
//         L: 50,
//         C: 100,
//         D: 500,
//         M: 1000
//     }
//     let sum = 0
//     for (var i = 0; i < s.length; i++) {
//         let val = s[i];
//         // console.log(val);
//         sum += obj[val];
//         // console.log(sum);
//         if (!!s[i - 1]) {
//             if ((val == "V" || val == "X") && s[i - 1] == "I") {
//                 sum -= 2
//             } else if ((val == "L" || val == "C") && s[i - 1] == "X") {
//                 sum -= 20
//             } else if ((val == "D" || val == "M") && s[i - 1] == "C") {
//                 sum -= 200
//             }
//         }
//     }
//     console.log(sum);
//     return sum
// };

// romanToInt(s)


// //最长公共前缀
// let strs = ["flower", "flow", "flight"];

// function longString(strs) {
//     // console.log(strs);
//     var l = strs[0];
//     var result = "";
//     if (strs.length == 0) {
//         return result
//     }
//     for (var i = 0; i < l.length; i++) {
//         for (var j = 1; j < strs.length; j++) {
//             if (l[i] != strs[j][i]) {
//                 return result
//             }
//         }  
//         result += l[i]
//         console.log(result);
//     }

//     return result
// }
// longString(strs)



// //有效的括号

// let s = "()[]{}"
// function isValid(s) {
//     // console.log(s);
//     let map = {
//         "{": "}",
//         "(": ")",
//         "[": "]",
//     }
//     let stack = [];
//     for (let i = 0; i < s.length; i++) {
//         // console.log(s[i]);
//         if (map[s[i]]) {
//             stack.push(s[i]);
//         } else if (s[i] !== map[stack.pop()]) {
//             return false
//         }
//     }
//     return stack.length === 0;
// }

// isValid(s)

//移除元素

// let nums = [0, 1, 1, 2, 3, 4, 5, 6, 6, 2, 3];
// let val = 2;
// function removeElement(nums, val) {
//     var index = 0
//     for (let i = 0; i < nums.length; i++) {
//         if (nums[i] != val) {
//             nums[index] = nums[i]
//             index++
//         }
//     }
//     console.log(nums);
//     return index
// }
// removeElement(nums, val)


// //两个数组的交集 II
// let nums1 = [1, 2, 2, 1], nums2 = [2, 2]
// function intersect(nums1, nums2) {
//     let i = j = 0,
//         len1 = nums1.length,
//         len2 = nums2.length,
//         newArr = [];

//     if (len1 === 0 || len2 === 0) {
//         return newArr;
//     }

//     nums1.sort(function (a, b) {
//         return a - b;
//     });
//     nums2.sort(function (a, b) {
//         return a - b;
//     });

//     while (i < len1 || j < len2) {
//         if (nums1[i] > nums2[j]) {
//             j++;
//         } else if (nums1[i] < nums2[j]) {
//             i++;
//         } else {

//             if (nums1[i] === nums2[j]) {
//                 newArr.push(nums1[i]);
//             }

//             if (i < len1 - 1) {
//                 i++;
//             } else {
//                 break;
//             }

//             if (j < len2 - 1) {
//                 j++;
//             } else {
//                 break;
//             }
//         }
//     }
//     console.log(newArr);
//     return newArr;

// }
// intersect(nums1, nums2)


// //只出现一次的数字
// let nums = [4, 1, 2, 1, 2]
// function singleNumber(nums) {
//     for (var i = 1; i < nums.length; i++) {
//         nums[0] = nums[0] ^ nums[i];
//     }
//     return nums[0]
// }
// singleNumber(nums)




// // 实现 strStr()
// let haystack = "hello", needle = "ll"
// function strStr(haystack, needle) {
//     if (needle === "") {
//         return 0
//     }
//     for (let i = 0; i < haystack.length; i++) {
//         if (haystack[i] === needle[0]) {
//             if (haystack.substring(i, i + needle.length) === needle) {
//                 return i
//             }
//         }
//     }
//     return -1
// }
// strStr(haystack, needle);

// //搜索插入位置
// let nums = [1, 3, 5, 6], target = 5
// function searchInsert(nums, target) {
//     var len = nums.length;
//     for (var i = 0; i < len; i++) {
//         if (nums[i] === target) { //数组中有目标数
//             return i;
//         } else if (nums[i] > target) { //目标数应插入数组中
//             var temp = target;
//             for (var j = i - 1; j < len; j++) {
//                 var tempp = nums[j];
//                 nums[j] = temp;
//                 temp = nums[j];
//                 nums[j + 1] = tempp;
//             }
//             return i;
//         }
//     }
//     //目标数比数组中的数都大
//     nums[len] = target;
//     return len;
// }
// searchInsert(nums, target);


// //最后一个单词的长度
// let s = "Hello World"
// function lengthOfLastWord(s) {
//     if (!s) {
//         return 0;
//     }
//     let arr = s.trim().split(" ")
//     return arr[arr.length - 1].length
// }
// lengthOfLastWord(s)


// //加一
// let digits = [4, 3, 2, 1]
// function plusOne(digits) {
//     var len = digits.length;
//     for (let i = len - 1; i >= 0; i--) {
//         if (digits[i] < 9) {
//             digits[i]++;
//             return digits;
//         }
//         digits[i] = 0;
//     }
//     return [1, ...digits];
// }
// plusOne(digits)


// //二进制求和
// let a = 10, b = 10;
// function addBinary(a, b) {
//     //对长度较短的前面进行补0
//     if (a.length != b.length) {
//         let str = ''
//         let length = Math.abs(a.length - b.length);
//         for (let i = 0; i < length; i++) {
//             str += '0';
//         }
//         if (a.length > b.length) {
//             b = str + b;
//         } else {
//             a = str + a;
//         }
//     }
//     let num = 0; //记录是否有进位
//     let c = '';
//     for (let i = a.length - 1; i >= 0; i--) {
//         if (a[i] == b[i]) {
//             if (num == 1) {
//                 c = '1' + c; //二进制1+1+1 个位等于1,num = 1保留不变
//             } else {
//                 c = '0' + c;
//             }
//             if (a[i] == 0) {
//                 num = 0; //0+0+1 = 1,num被消耗掉一位;
//             } else {
//                 num = 1; //进一位;
//             }
//         } else {
//             //相同位,值不等,则情况与上边相反;
//             if (num == 1) {
//                 c = '0' + c;
//             } else {
//                 c = '1' + c
//             }
//         }

//     }
//     //最后如果num==1,则有进位,就需要在字符c前面+1
//     console.log(num);
//     return num === 1 ? '1' + c : c;
// }
// addBinary(a, b)



// //x 的平方根
// let x=4
// function mySqrt(x){
//     console.log(Math.floor(Math.sqrt(x)));
//     return Math.floor(Math.sqrt(x))
// }
// mySqrt(x);


// //二叉树的中序遍历
// let root = [1, null, 2, 3]
// function inorderTraversal(root) {
//     if (!root) {
//         return []
//     }
//     var res = [];
//     inorder(root, res);
//     return res;
// }
// function inorder(root, res) {
//     if (!root) {
//         return
//     }
//     inorder(root.left, res);
//     res.push(root.val);
//     inorder(root.right, res);
// }
// inorderTraversal(root)


// //买卖股票的最佳时机
// let prices = [7, 6, 4, 3, 1];
// function maxProfit(prices) {
//     if (!prices) return 0;
//     let temp = prices[0];
//     let sub = 0;
//     for (let i = 1; i < prices.length; i++) {
//         if (prices[i] < temp) {
//             temp = prices[i];
//         } else if (prices[i] - temp > sub) {
//             sub = prices[i] - temp;
//         }
//     }
//     return sub
// }
// maxProfit(prices)


// //两数之和 II - 输入有序数组
// let nums = [2, 3, 4], target = 6
// function twoSum(nums, target) {
//     var len = nums.length
//     for (var i = 0; i < len; i++) {
//         for (var x = i + 1; x < len; x++) {
//             if (target - nums[i] == nums[x]) {
//                 console.log(i+1,x+1);
//                 return [i + 1, x + 1]
//             }
//         }
//     }
// }
// twoSum(nums, target);


// //Excel表列名称
// let columnNumber = 28
// function convertToTitle(columnNumber) {
//     const nums = [];
//     let code = 'A'.charCodeAt(0)
//     for (let i = code; i < code + 26; i++) {
//         nums.push(String.fromCharCode(i));
//     }
//     let ans = [];
//     let index = 0;
//     do {
//         columnNumber -= 1;
//         ans.unshift(nums[columnNumber % 26]);
//     } while (columnNumber = Math.floor(columnNumber / 26));
//     return ans.join("")

// };
// convertToTitle(columnNumber)

// //多数元素
// let nums = [2, 2, 1, 1, 1, 2, 2];
// var majorityElement = function (nums) {
//     nums.sort((a, b) => a - b);
//     return nums[Math.floor(nums.length / 2)]
// };
// majorityElement(nums)

//Excel 表列序号
// let columnTitle="A"
// var titleToNumber = function (columnTitle) {
//     var maps = {
//         "A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8, "I": 9,
//         "J": 10, "K": 11, "L": 12, "M": 13, "N": 14, "O": 15, "P": 16, "Q": 17,
//         "R": 18, "S": 19, "T": 20, "U": 21, "V": 22, "W": 23, "X": 24, "Y": 25,
//         "Z": 26
//     }
//     var sum = 0;
//     for (var i = columnTitle.length - 1, y = 1; i > -1; i--, y *= 26) {
//         sum += maps[columnTitle[i]] * y
//     }
//     console.log(sum);
//     return sum
// };
// titleToNumber(columnTitle)


// //阶乘后的零
// let n = 5;
// var trailingZeroes = function (n) {
//     let num = 0;
//     while (n >= 5) {
//         n = Math.floor(n / 5);
//         num += n
//     }
//     return num
// };
// trailingZeroes(n);


// //快乐数
// let n=19;
// var isHappy = function (n) {
//     let mem = {}
//     let s = n + ''
//     while (s !== '1') {
//         if (mem[s]) return false
//         mem[s] = true
//         let sum = 0
//         for (let i = 0; i < s.length; i++) {
//             sum += s[i] * s[i]
//         }
//         s = sum + ''
//     }
//     return true
// };
// isHappy(n);


// //计算质数
// let n=10;
// var countPrimes = function (n) {
//     if (n <= 1) {
//         return 0
//     }
//     let count = 0;
//     for (let i = 2; i < n; i++) {
//         if (i > 3 && (i % 2 === 0 || i % 3 === 0)) {
//             continue
//         }
//         if (isPrime(i)) {
//             count++
//         }
//     }
//     return count
//     function isPrime(num) {
//         let p = Math.sqrt(num);
//         for (let i = 2; i <= p; i++) {
//             if (num % i === 0) {
//                 return false
//             }
//         }
//         return true
//     }
// };
// countPrimes(n);


// //同构字符串
// let s = "egg", t = "add"
// var isIsomorphic = function (s, t) {
//     let i = 0;
//     while (i < s.length) {
//         if (s.indexOf(s[i]) != t.indexOf(t[i])) {
//             return false
//         }
//         i++
//     }
//     return true
// };
// isIsomorphic(s, t);



// //各位相加
// let num = 38
// var addDigits = function (num) {
//     if (num < 9) {
//         return num
//     }
//     num = num % 9;
//     return num == 0 ? 9 : num
// };
// addDigits(num);


// //丑数
// let n=6
// var isUgly = function(n) {
//     if (n === 0) return false;
//    while (n%2 === 0) {
//        n = n / 2;
//    }
//    while (n%3 === 0) {
//        n = n / 3;
//    }
//    while (n%5 === 0) {
//        n = n / 5;
//    }
//    return (n===2 || n === 3|| n === 1|| n === 5);
// };
// isUgly(n);


// //丢失的数字
// let nums=[1,0,3];
// var missingNumber = function (nums) {
//     let len = nums.length,
//         sum = ((1 + len) * len) / 2

//     for (const i of Array(len).keys()) {
//         sum -= nums[i]
//     }
//     return sum
// }
// missingNumber(nums);


// //移动零
// let nums = [1, 0, 2, 0, 4, 0, 7];
// var moveZeroes = function (nums) {
//     let len = nums.length - 1;
//     for (let i = len; i >= -0; i--) {
//         if (nums[i] == 0) {
//             nums.push(nums.splice(i, 1));
//         }
//     }
//     return nums
// };
// console.log(moveZeroes(nums));



// //单词规律
// let pattern = "abba", str = "dog cat cat dog"
// var wordPattern = function(pattern, s) {
//     var arr = s.split(' ');
//     if(pattern.length!=arr.length) return false;
//     for(var i=0;i<pattern.length;i++){
//         if(pattern.indexOf(pattern[i])!=arr.indexOf(arr[i])){
//             return false;
//         }
//     }
//     return true;
// };
// wordPattern(pattern, str)


// //3的幂
// let n = 27
// var isPowerOfThree = function (n) {
//     if (n < 1) {
//         return false
//     }
//     while (n % 3 == 0) {
//         n = n / 3
//     }
//     return n == 1
// };
// isPowerOfThree(n);


// //4的幂
// let n = 16
// var isPowerOfFour = function (n) {
//     return n > 0 && !((n - 1) & n) && n.toString(2).length % 2
// };
// isPowerOfFour(n);

// //反转字符串
// let s=["a","s","d","f","g","h","j","k","l"];
// var reverseString = function(s) {
//     return s.reverse();
// };
// reverseString(s);



// //有效的完全平方数
// let num = 16
// var isPerfectSquare = function (num) {
//     if (num === 0 || num === 1) {
//         return true
//     }
//     let left = 1, right = num
//     while (left <= right) {
//         let mid = (left + right) >> 1;
//         if (mid * mid === num) {
//             return true
//         } else if (mid * mid > num) {
//             right = mid - 1
//         } else {
//             left = mid + 1
//         }
//     }
//     return false
// };
// isPerfectSquare(num);



// //赎金信
// let ransomNote = "aa", magazine = "aab"
// var canConstruct = function (ransomNote, magazine) {
//     let len = ransomNote.length;
//     magazine = magazine.split('');
//     for (let i = 0; i < len; i++) {
//         let index = magazine.indexOf(ransomNote[i]);
//         if (index === -1) {
//             return false
//         }
//         if (index !== -1) {
//             magazine.splice(index, 1);
//         }
//     }
//     return true
// };
// canConstruct(ransomNote, magazine);


// //第三大的数
// let nums = [3, 2, 1];
// var thirdMax = function (nums) {
//     var arr = [...new Set(nums)].sort((a, b) => b - a);
//     return arr.length >= 3 ? arr[2] : arr[0];
// };
// thirdMax(nums);


//